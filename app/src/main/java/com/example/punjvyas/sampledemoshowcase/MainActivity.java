package com.example.punjvyas.sampledemoshowcase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class MainActivity extends AppCompatActivity {

    Button call, help, repeat, alert, pic;
    private static final String SHOWCASE_ID = "custom example";
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        help = (Button) findViewById(R.id.b1);
        call = (Button) findViewById(R.id.b2);
        repeat = (Button) findViewById(R.id.b3);
        alert = (Button) findViewById(R.id.b4);
        pic = (Button) findViewById(R.id.b5);

         prefs = PreferenceManager.getDefaultSharedPreferences(this);


        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!prefs.getBoolean("firstTime", false)) {
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putBoolean("firstTime", true);
                    editor1.apply();
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(help)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Learn to excess Application")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                } else
                    Toast.makeText(MainActivity.this, "Click help", Toast.LENGTH_LONG).show();
            }


        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!prefs.getBoolean("firstTimeCall", false)) {
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putBoolean("firstTimeCall", true);
                    editor1.apply();
                    Toast.makeText(MainActivity.this, "Click inside if", Toast.LENGTH_LONG).show();
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(help)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Learn to excess Application")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                } else
                    Toast.makeText(MainActivity.this, "Click call", Toast.LENGTH_LONG).show();
            }
        });

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!prefs.getBoolean("firstTimeRepeat", false)) {
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putBoolean("firstTimeRepeat", true);
                    editor1.apply();

                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(view)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Repeat Your Order")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }

                else  {
                    Intent intent = new Intent(MainActivity.this, Home.class);
                    startActivity(intent);

                }
            }
        });

        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefs.getBoolean("firstPic", false)) {
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putBoolean("firstPic", true);
                    editor1.apply();

                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(pic)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Upload Your prescription here")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                } else {

                    Toast.makeText(MainActivity.this, "Click call", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

}








       /*if (!prefs.getBoolean("firstTime", false)) {


            // <---- run your one time code here
            help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(help)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Learn to excess Application")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });

            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(call)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Call Us")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });

            // <---- run your one time code here
            help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(help)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Learn to excess Application")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });


            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(call)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Call Us")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });


            repeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)

                            .setTarget(view)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Repeat Your Order")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });


            alert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(alert)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Get Notification Here")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });

            pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MaterialShowcaseView.Builder(MainActivity.this)
                            .setTarget(pic)
                            .setShapePadding(10)
                            .setDismissText("GOT IT")
                            .setContentText("Upload Your prescription here")
                            .setContentTextColor(getResources().getColor(R.color.green))
                            .setMaskColour(getResources().getColor(R.color.blue))
                            .show();
                }
            });

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", true);
            editor.commit();
        }
    }*/




